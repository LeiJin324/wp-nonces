<?php
/**
 * WPNonce Test Case 
 *
 * @category  WPNonce
 * @author    JinLei <xiaojinteacher@gmail.com>
 * @license   GPLv2+
 */

declare(strict_types=1);

use JinLei\WPNonce;
use JinLei\WPNonceService;
use PHPUnit\Framework\TestCase;


/**
 * WPNonceServie implemented
 * PHP version 7
 *
 * LICENSE: GPLv2+
 * @author     JinLei <xiaojinteacher@gmail.com>
 * @license    GPLv2+
 */
class DIWPNonceService implements WPNonceService
{
    public function wp_nonce_ays(string $action='log-out'):int
    {
        return 1;
    }
    public function wp_nonce_field(int $action,string $name,bool $referer,bool $echo ):string
    {
        return "wp_nonce_field";
    }
    
    public function wp_nonce_url(string $actionurl='http://site.ru/url', int $action,string $name ):string 
    {
        return $actionurl;
    }
    
    public function wp_verify_nonce(string $nonce='my-nonce', int $action ):int 
    {
        return 1;
    }
    
    public function wp_create_nonce(int $action):string 
    {
        return "wp_create_nonce";
    }
    
    public function check_admin_referer(int $action,string $query_arg):int 
    {
        return 1;
    }
    
    public function check_ajax_referer(int $action,string $query_arg,bool $die):int 
    {
        return 1;
    }
    
    public function wp_referer_field(bool $echo ):string 
    {
        return "wp_referer_field";
    }
}


/**
 * WPNonce Test Case Class 
 * PHP version 7
 *
 * @author     JinLei <xiaojinteacher@gmail.com>
 * @license    GPLv2+
 */
final class WPNonceTest extends TestCase
{
    private $wpNonceService;

    public function setUp():void
    {
        parent::setUp();
        $this->wpNonceService = new DIWPNonceService();
    }
    public function test_wp_nonce_ays(string $action='log-out'):void
    {
        $WPNonce = new WPNonce($this->wpNonceService);
        $nonceAys = $WPNonce -> wp_nonce_ays($action='log-out');
        $this->assertEquals(1, $nonceAys);
    }

    public function test_wp_nonce_field():void 
    {
        $WPNonce = new WPNonce($this->wpNonceService);
        $nonceField = $WPNonce -> wp_nonce_field();
        $this->assertEquals("wp_nonce_field", $nonceField);
    }

    public function test_wp_nonce_url(string $actionurl='http://site.ru/url'):void 
    {
        $WPNonce = new WPNonce($this->wpNonceService);
        $nonceUrl = $WPNonce -> wp_nonce_url($actionurl='http://site.ru/url');
        $this->assertEquals("http://site.ru/url", $nonceUrl);
    }

    public function test_wp_verify_nonce(string $nonce='my-nonce'):void 
    {
        $WPNonce = new WPNonce($this->wpNonceService);
        $nonceVerify = $WPNonce -> wp_verify_nonce($nonce='my-nonce');
        $this->assertEquals(1, $nonceVerify);
    }

    public function test_wp_create_nonce():void 
    {
        $WPNonce = new WPNonce($this->wpNonceService);
        $nonceCreate = $WPNonce -> wp_create_nonce();
        $this->assertEquals("wp_create_nonce", $nonceCreate);
    }

    public function test_check_admin_referer():void 
    {
        $WPNonce = new WPNonce($this->wpNonceService);
        $checkAdminRefer = $WPNonce -> check_admin_referer();
        $this->assertEquals(1, $checkAdminRefer);
    }

    public function test_check_ajax_referer():void 
    {
        $WPNonce = new WPNonce($this->wpNonceService);
        $checkAjaxRefer = $WPNonce -> check_ajax_referer();
        $this->assertEquals(1, $checkAjaxRefer);
    }

    public function test_wp_referer_field():void 
    {
        $WPNonce = new WPNonce($this->wpNonceService);
        $wpReferField = $WPNonce -> wp_referer_field();
        $this->assertEquals("wp_referer_field", $wpReferField);
    }

    public function tearDown():void
    {
        parent::tearDown();
        $this->wpNonceService = null;
    }

}


