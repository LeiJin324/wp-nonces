# Changelog
All notable changes to this project will be documented in this file.


## [0.4.0] - 2020-01-13
### Changed
- Test Case Code Style

## [0.4.0] - 2020-01-13
### Changed
- Code Style

## [0.3.0] - 2020-01-12
### Added
- Dependency Injection

## [0.2.0] - 2020-01-12
### Added
- Add WPNonce class methods implements Nonce.
- Add WPNonce Test case to test WPNonce class
### TODO
- Dependency Injection
- Code Style

## [0.1.1] - 2020-01-12
### Changed
- composer.json Add PHP Code Style

## [0.1.0] - 2020-01-12
### Added 
- First Release
